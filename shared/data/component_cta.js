export const Data = [
  {
    title: "Waar kunnen we u mee helpen?",
    imgurl: "/cta.png",
    imgalt: "Call to action image",
    info: [{ 
      icon: "mail.svg",
      text: "delo@boringen.be"
    },
    { 
      icon: "telephone.svg",
      text: "0032 326 22 48"
    }]
  }
]; 