export const Data = [
  {
    title: "Leer ons kennen",
    intro: "Délo Boringen bvba werd opgericht in 2003 en is op korte tijd uitgegroeid tot één van de grootste milieukundige boorbedrijven in Vlaanderen.\n U kan bij ons terecht voor àl uw milieukundige boringen.",
    text: "19 veldwerkers, 6 volledig uitgeruste boorwagens, 2 grondwaterbemonsteringswagens en 3 vrachtwagens uitgerust met boortorens die dagelijks worden ingezet in heel België, Luxemburg of Noord-Frankrijk.\n \n Délo Boringen bvba is meer dan alleen een grondboorbedrijf. Wij bieden u een vlotte dienstverlening, kwaliteitsvol werk en een klantgerichte service, op alle gebied.",
  }, 
]; 