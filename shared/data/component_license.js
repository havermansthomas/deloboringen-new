export const Data = [
  {
    title: "Licentie Vlaanderen",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tincidunt, orci a mollis faucibus, risus arcu placerat est, scelerisque gravida eros urna eget lectus. Aenean tincidunt vulputate lacus, eget consectetur nibh mattis eu. Suspendisse fermentum velit in viverra vehicula. Quisque fringilla orci fermentum, tempus mi sit amet, venenatis lorem. Nullam sit amet neque maximus, dapibus justo eget, malesuada odio. Vivamus molestie ligula nisl, eget vehicula est pulvinar nec. Ut ullamcorper consectetur viverra. Mauris vitae risus massa. Nam condimentum, mauris et consequat sagittis, nibh risus tristique ipsum, a semper lacus felis in justo.",
    imgurl: "/licentie.png",
    imgalt: "Licentie alt tekst"
  }, 
  {
    title: "Licentie Wallonië",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tincidunt, orci a mollis faucibus, risus arcu placerat est, scelerisque gravida eros urna eget lectus. Aenean tincidunt vulputate lacus, eget consectetur nibh mattis eu. Suspendisse fermentum velit in viverra vehicula. Quisque fringilla orci fermentum, tempus mi sit amet, venenatis lorem. Nullam sit amet neque maximus, dapibus justo eget, malesuada odio. Vivamus molestie ligula nisl, eget vehicula est pulvinar nec. Ut ullamcorper consectetur viverra. Mauris vitae risus massa. Nam condimentum, mauris et consequat sagittis, nibh risus tristique ipsum, a semper lacus felis in justo.",
    imgurl: "/licentie.png",
    imgalt: "Licentie alt tekst"
  },
  {
    title: "Licentie Brussel",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tincidunt, orci a mollis faucibus, risus arcu placerat est, scelerisque gravida eros urna eget lectus. Aenean tincidunt vulputate lacus, eget consectetur nibh mattis eu. Suspendisse fermentum velit in viverra vehicula. Quisque fringilla orci fermentum, tempus mi sit amet, venenatis lorem. Nullam sit amet neque maximus, dapibus justo eget, malesuada odio. Vivamus molestie ligula nisl, eget vehicula est pulvinar nec. Ut ullamcorper consectetur viverra. Mauris vitae risus massa. Nam condimentum, mauris et consequat sagittis, nibh risus tristique ipsum, a semper lacus felis in justo.",
    imgurl: "/licentie.png",
    imgalt: "Licentie alt tekst"
  },
]; 