export const Data = [
  {
    title: "Handmatige boringen",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tincidunt, orci a mollis faucibus, risus arcu placerat est, scelerisque gravida eros urna eget lectus. Aenean tincidunt vulputate lacus, eget consectetur nibh mattis eu. Suspendisse fermentum velit in viverra vehicula. Quisque fringilla orci fermentum, tempus mi sit amet, venenatis lorem. Nullam sit amet neque maximus, dapibus justo eget, malesuada odio. Vivamus molestie ligula nisl, eget vehicula est pulvinar nec. Ut ullamcorper consectetur viverra. Mauris vitae risus massa. Nam condimentum, mauris et consequat sagittis, nibh risus tristique ipsum, a semper lacus felis in justo.",
    imgurl: "/boortechniek1.png",
    imgalt: "Boortechniek alt tekst"
  }, 
  {
    title: "Machinale boringen",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tincidunt, orci a mollis faucibus, risus arcu placerat est, scelerisque gravida eros urna eget lectus. Aenean tincidunt vulputate lacus, eget consectetur nibh mattis eu. Suspendisse fermentum velit in viverra vehicula. Quisque fringilla orci fermentum, tempus mi sit amet, venenatis lorem. Nullam sit amet neque maximus, dapibus justo eget, malesuada odio. Vivamus molestie ligula nisl, eget vehicula est pulvinar nec. Ut ullamcorper consectetur viverra. Mauris vitae risus massa. Nam condimentum, mauris et consequat sagittis, nibh risus tristique ipsum, a semper lacus felis in justo.",
    imgurl: "/boortechniek2.png",
    imgalt: "Boortechniek alt tekst"
  },
  {
    title: "Grondwaterstaalname",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tincidunt, orci a mollis faucibus, risus arcu placerat est, scelerisque gravida eros urna eget lectus. Aenean tincidunt vulputate lacus, eget consectetur nibh mattis eu. Suspendisse fermentum velit in viverra vehicula. Quisque fringilla orci fermentum, tempus mi sit amet, venenatis lorem. Nullam sit amet neque maximus, dapibus justo eget, malesuada odio. Vivamus molestie ligula nisl, eget vehicula est pulvinar nec. Ut ullamcorper consectetur viverra. Mauris vitae risus massa. Nam condimentum, mauris et consequat sagittis, nibh risus tristique ipsum, a semper lacus felis in justo.",
    imgurl: "/boortechniek3.png",
    imgalt: "Boortechniek alt tekst"
  },
  {
    title: "Asbest staalname",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tincidunt, orci a mollis faucibus, risus arcu placerat est, scelerisque gravida eros urna eget lectus. Aenean tincidunt vulputate lacus, eget consectetur nibh mattis eu. Suspendisse fermentum velit in viverra vehicula. Quisque fringilla orci fermentum, tempus mi sit amet, venenatis lorem. Nullam sit amet neque maximus, dapibus justo eget, malesuada odio. Vivamus molestie ligula nisl, eget vehicula est pulvinar nec. Ut ullamcorper consectetur viverra. Mauris vitae risus massa. Nam condimentum, mauris et consequat sagittis, nibh risus tristique ipsum, a semper lacus felis in justo.",
    imgurl: "/boortechniek4.png",
    imgalt: "Boortechniek alt tekst"
  }
]; 