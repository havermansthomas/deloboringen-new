import React from 'react';
import Hero from '../components/hero/hero-nrml';
import Subscribe from '../components/subscribe/subscribe';

import {Data} from '../shared/data/page_contact';

function About() {
  return (
    <React.Fragment>
      <Hero hero_data={Data} />
      <Subscribe />
    </React.Fragment>
  ) 
}



export default About