import React from 'react';
import HeroImg from '../components/hero/hero-img';
import Technieken from '../components/technieken/technieken';
import License from '../components/License/License';
import About from '../components/about/about';
import Cta from '../components/cta/cta';
import Subscribe from '../components/subscribe/subscribe';

function Home() {
  return (
    <React.Fragment>
      <HeroImg />
      <Technieken />
      <License />
      <About />
      <Cta />
      <Subscribe />
    </React.Fragment>
  ) 
}

export default Home