import React from 'react';
import Hero from '../components/hero/hero-nrml';
import About_us from '../components/about/about';
import Cta from '../components/cta/cta';
import Subscribe from '../components/subscribe/subscribe';

import {Data} from '../shared/data/page_about';

function About() {
  return (
    <React.Fragment>
      <Hero hero_data={Data} />
      <About_us />
      <Cta />
      <Subscribe />
    </React.Fragment>
  ) 
}



export default About