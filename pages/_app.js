import React from 'react';
import '../shared/styles/globals.css';
import Header from '../components/navigation/header';
import Footer from '../components/navigation/footer';

function MyApp({ Component, pageProps }) {
  return (
    <React.Fragment>
      <Header/>
      <Component {...pageProps} />
      <Footer/>
    </React.Fragment>
  )
}

export default MyApp
