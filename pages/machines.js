import React from 'react';
import Hero from '../components/hero/hero-nrml';
import Cta from '../components/cta/cta';
import Subscribe from '../components/subscribe/subscribe';

import {Data} from '../shared/data/page_machines';

function About() {
  return (
    <React.Fragment>
      <Hero hero_data={Data} />
      <Cta />
      <Subscribe />
    </React.Fragment>
  ) 
}



export default About