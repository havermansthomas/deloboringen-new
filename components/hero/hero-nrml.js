import React, { Component } from 'react';
import Page from '../../shared/styles/page.module.scss';
import Styles from '../../shared/styles/hero.module.scss';

class Hero extends Component {
  render () {
    return (
      <React.Fragment>
        <section>
          {this.props.hero_data.map((data, key) => {
            return (
              <>
                  <div className={`${Styles.hero} ${Styles.heroleft} ${Page.bg}`} key={key}>
                    <h1 className={`${Page.padding} ${Page.h1} ${Page.inner} ${Styles.title}`}>{data.title}</h1>
                  </div>
              </>
            )
          })}
        </section>
      </React.Fragment>
    )
  }
}

export default Hero;