import React from 'react';
import {Data} from '../../shared/data/homepage-hero';
import Styles from '../../shared/styles/hero.module.scss';
import Page from '../../shared/styles/page.module.scss';

function HeroImg () {
  return (
    <React.Fragment>
      {Data.map((data, key) => {
        return (
          <>
            <section key={key}>
              <div className={Styles.hero}>
                <img className={Styles.img} src={data.imgurl} alt={data.imgalt}/>
                <h1 className={`${Page.padding} ${Page.h1} ${Page.whitecolor} ${Page.inner}`}>{data.title}</h1>
              </div>
            </section>
          </>
        )
      })}

    </React.Fragment>
  )
}



export default HeroImg;