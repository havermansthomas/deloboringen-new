import React from 'react';
import {Data} from '../../shared/data/component_license';
import Styles from '../../shared/styles/cards.module.scss';
import Page from '../../shared/styles/page.module.scss';

function License () {
  return (
    <React.Fragment>
      <section className={`${Page.constrict} ${Page.halfbg}`}>
        
        <div className={`${Page.inner} ${Page.padding}`}>
          <h2 className={Page.h2}>Licenties om te boren</h2>

          <div className={`${Styles.cards} ${Styles.cardsleft}`}>
            {Data.map((data, key) => {
              return (
                <>
                  <figure key={data.key} className={Styles.item}>
                    <img src={data.imgurl}/>
                    <p>{data.title}</p>
                  </figure>
                </>
              )
            })}
          </div>
        </div>

      </section>
    </React.Fragment>
  )
}

export default License