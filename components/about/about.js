import React from 'react';
import Page from '../../shared/styles/page.module.scss';
import {Data} from '../../shared/data/component_about';

function About () {
  return (
    <React.Fragment>

      <section className={Page.constrict}>
        <div className={`${Page.inner} ${Page.padding}`}>
          {Data.map((data, key) => {
            return (
              <>
                <h2 className={Page.h2}>{data.title}</h2>
                <p className={Page.intro}>{data.intro.split('\n').map((intro, key) => {return <span key={key}>{intro}<br/></span>})}</p>
                <p>{data.text.split('\n').map((text, key) => {return <span key={key}>{text}<br/></span>})}</p>
              </>
            )
          })}
        </div>
      </section>

    </React.Fragment>
  )
}

export default About;