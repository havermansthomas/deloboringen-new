import React from 'react';
import Page from '../../shared/styles/page.module.scss';
import Styles from '../../shared/styles/cta.module.scss';
import {Data} from '../../shared/data/component_cta'

function Cta () {
  return (
    <React.Fragment>
      {Data.map((data, key) => {
        return (
          <>
            <section className={Styles.container} key={key} >
              <div className={`${Styles.inner} ${Styles.img}`}>
                <img alt={data.imgalt} src={data.imgurl} />
              </div>

              <div className={Styles.inner}>
                <div className={Styles.innerContent}>
                  <h2 className={Page.h2}>{data.title}</h2>
                  {data.info.map((item, i) => {
                    return <div className={Styles.info} key={i}>
                      <img alt="icon" src={item.icon} />
                      <p>{item.text}</p>
                    </div>
                  })}
                </div>
              </div>
            </section>
          </>
        )
      })}
    </React.Fragment>
  )
}

export default Cta;