import React from 'react';
import Link from 'next/link';
import Styles from '../../shared/styles/header.module.scss';
import Page from '../../shared/styles/page.module.scss';

function Header() {
  return (
    <React.Fragment>
      <header className={Styles.header}>
        <div className={Styles.inner}>
          <div>
            <Link href="/">
              <a><img src="/logo.png" /></a>
            </Link>
          </div>

          <ul className={Styles.links}>
            <li>
              <Link href="/over-ons">
                <a className={Page.underline}>Over ons</a>
              </Link>
            </li>
            <li>
              <Link href="/machines">
                <a className={Page.underline}>Machines</a>
              </Link>
            </li>
            <li>
              <Link href="/contact">
                <a className={Page.underline}>Contact</a>
              </Link>
            </li>
          </ul>
        </div>
      </header>
    </React.Fragment>
  )
}

export default Header