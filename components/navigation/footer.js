import React from 'react';
import Link from 'next/link';
import Styles from '../../shared/styles/footer.module.scss';
import Page from '../../shared/styles/page.module.scss';


function Footer() {
  return (
    <React.Fragment>
      <footer className={`${Styles.footer} ${Page.brownbg} ${Page.constrict}`}>
        <div className={`${Page.inner} ${Page.padding} ${Styles.inner}`}>
          <div>
            <ul className={Styles.content}>
              <h4 className={Page.h4}>Delo boringen</h4>
              <li>Industriepark Brechtsebaan 18A, 2900 Schoten</li>
              <li>0475 59 42 72</li>
              <li><Link href="/contact"><a className={Page.underline}>delo@boringen.be</a></Link></li>
            </ul>
          </div>

          <div className={Styles.column}>
            <ul className={Styles.content}>
              <p className={Styles.subtitle}>Algemeen</p>
              <li>
                <Link href="/">
                  <a className={Page.underline}>Home</a>
                </Link>
              </li>
              <li>
                <Link href="/over-ons">
                  <a className={Page.underline}>Over ons</a>
                </Link>
              </li>
              <li>
                <Link href="/machines">
                  <a className={Page.underline}>Machines</a>
                </Link>
              </li>
              <li>
                <Link href="/contact">
                  <a className={Page.underline}>Contact</a>
                </Link>
              </li>
            </ul>

            <ul className={Styles.content}>
              <p className={Styles.subtitle}>Meer informatie</p>
              <li>
                <Link href="/voorwaarden">
                  <a className={Page.underline}>Algemene voorwaarden</a>
                </Link>
              </li>
              <li>
                <Link href="/policy">
                  <a className={Page.underline}>Privacy policy</a>
                </Link>
              </li>
            </ul>

            <ul className={Styles.content}>
              <p className={Styles.subtitle}>Social media</p>
              <div className={Styles.socials}>
                <a href="#"><img alt="icon" src="/facebook-icn.svg" /></a>
                <a href="#"><img alt="icon" src="/instagram-icn.svg" /></a>
                <a href="#"><img alt="icon" src="/linkedin-icn.svg" /></a>
              </div>
            </ul>
          </div>
        </div>
      </footer>
    </React.Fragment>
  )
}

export default Footer