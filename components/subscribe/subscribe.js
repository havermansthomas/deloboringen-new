import React from 'react';
import Page from '../../shared/styles/page.module.scss';
import Styles from '../../shared/styles/subscribe.module.scss';


function Subscribe () {
  return (
    <React.Fragment>
      <section className={`${Page.constrict} ${Page.brownbg} ${Page.padding}`}>
        <div className={Styles.inner}>
          <h2 className={Page.h3}>Blijf op de hoogte van ons!</h2>

          <form className={Styles.subscribe}>
            <input type="text" className={Styles.input} name="email" placeholder="Uw email adres" />
            <input type="submit" className={Styles.submit} name="submit" value="Verzenden" />
          </form>
        </div>

      </section>
    </React.Fragment>
  )
}

export default Subscribe;