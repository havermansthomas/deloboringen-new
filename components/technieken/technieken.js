import React from 'react';
import {Data} from '../../shared/data/component_technieken';
import Styles from '../../shared/styles/cards.module.scss';
import Page from '../../shared/styles/page.module.scss';

function Technieken () {
  return (
    <React.Fragment>
      <section className={`${Page.constrict} ${Page.bg}`}>
        
        <div className={`${Page.inner} ${Page.padding}`}>
          <h2 className={Page.h2}>Ontdek onze technieken</h2>

          <div className={Styles.cards}>
            {Data.map((data, key) => {
              return (
                <>
                  <figure key={data.key} className={Styles.item}>
                    <img src={data.imgurl}/>
                    <p>{data.title}</p>
                  </figure>
                </>
              )
            })}
          </div>
        </div>

      </section>
    </React.Fragment>
  )
}

export default Technieken;